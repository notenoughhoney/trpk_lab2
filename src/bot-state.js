module.exports = {
  isSetting: false,
  isSettingSpeed: false,
  isSettingLink: false,
  isSettingLimit: false,
  isSending: false,

  settings: {
    limit: 200,
    speed: 10,
    channel: "",
  },
  telephones: [],
};
