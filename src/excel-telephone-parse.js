const ExcelJS = require("exceljs");
const { checkTelephone } = require("./utils/check-telephone");

/**
 * Get telephones number from excel
 * @param {string} filepath Path to excel file
 * @param {number | string} woorksheetNumber Number of worksheet needed parse
 * @param {number | string} columnNumber Number of column with telephone number
 * @returns {string} Array of telephone number
 */
async function parseTelephoneExcel(filepath, woorksheetNumber, columnNumber) {
  const workbook = new ExcelJS.Workbook();
  await workbook.xlsx.readFile(filepath);

  const worksheet = workbook.getWorksheet(woorksheetNumber);
  const telephoneCol = worksheet.getColumn(columnNumber);

  const telephones = telephoneCol.values.filter(checkTelephone);
  return telephones;
}

module.exports = {
  parseTelephoneExcel,
};
