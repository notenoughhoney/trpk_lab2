const TelegramBot = require("node-telegram-bot-api");
const botState = require("./bot-state");
const conf = require("../conf");
const TEXTS = require("./texts");
const { parseTelephoneExcel } = require("./excel-telephone-parse");
const { downloadFileFromYandex } = require("./yandex-disk");
const { EXCEL_MIME_TYPE } = require("./utils/mime-types");
const fs = require("fs");
const {
  limitValidate,
  speedValidate,
  checkExistingChannel,
} = require("./utils/validators");

const token = conf.token;
const bot = new TelegramBot(token, { polling: true });

const EXCEL_WOOKSHEET_NUMBER = 1;
const EXCEL_COL_NUMBER = 1;
const FILES_SAVE_FOLDER = "./files";

// TODO: remove copy paste. ITS VERY VERY BAD

bot.onText(/\/start/, (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.WELCOME, {
    reply_markup: {
      keyboard: [[TEXTS.START], [TEXTS.LOAD], [TEXTS.SETTINGS]],
    },
  });
});

bot.onText(new RegExp(TEXTS.START), (msg) => {
  if (botState.isSetting) return;

  if (botState.telephones.length === 0) {
    bot.sendMessage(msg.chat.id, TEXTS.NO_NUMBER_PHONES);
    return;
  }
})

bot.onText(new RegExp(TEXTS.BACK), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.BACK_TEXT, {
    reply_markup: {
      keyboard: [[TEXTS.START], [TEXTS.LOAD], [TEXTS.SETTINGS]],
    },
  });
});

bot.onText(new RegExp(TEXTS.SETTINGS), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.SETTING_PROMPT, {
    reply_markup: {
      keyboard: [
        [TEXTS.SETTING_CURRENT, TEXTS.BACK],
        [TEXTS.SETTING_LINK],
        [TEXTS.SETTING_SPEED],
        [TEXTS.SETTING_LIMIT],
      ],
    },
  });
});

bot.onText(new RegExp(TEXTS.SETTING_CURRENT), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.SETTINGS_SHOW(), {
    parse_mode: "HTML",
  });
});

bot.onText(new RegExp(TEXTS.SETTING_SPEED), (msg) => {
  if (botState.isSetting) return;

  botState.isSetting = true;
  botState.isSettingSpeed = true;
  bot.sendMessage(msg.chat.id, TEXTS.SETTING_SPEED_DESCRIPTION, {
    reply_markup: {
      keyboard: [[TEXTS.BACK_TO_SETTINGS]],
    },
  });
});

bot.onText(new RegExp(TEXTS.SETTING_LINK), (msg) => {
  if (botState.isSetting) return;

  botState.isSetting = true;
  botState.isSettingLink = true;
  bot.sendMessage(msg.chat.id, TEXTS.SETTING_LINK_DESCRIPTION, {
    reply_markup: {
      keyboard: [[TEXTS.BACK_TO_SETTINGS]],
    },
  });
});

bot.onText(new RegExp(TEXTS.SETTING_LIMIT), (msg) => {
  if (botState.isSetting) return;

  botState.isSetting = true;
  botState.isSettingLimit = true;
  bot.sendMessage(msg.chat.id, TEXTS.SETTING_LIMIT_DESCRIPTION, {
    reply_markup: {
      keyboard: [[TEXTS.BACK_TO_SETTINGS]],
    },
  });
});

bot.onText(new RegExp(TEXTS.BACK_TO_SETTINGS), (msg) => {
  botState.isSetting = false;
  botState.isSettingLimit = false;
  botState.isSettingLink = false;
  botState.isSettingSpeed = false;

  bot.sendMessage(msg.chat.id, TEXTS.SETTING_PROMPT, {
    reply_markup: {
      keyboard: [
        [TEXTS.SETTING_CURRENT, TEXTS.BACK],
        [TEXTS.SETTING_LINK],
        [TEXTS.SETTING_SPEED],
        [TEXTS.SETTING_LIMIT],
      ],
    },
  });
});

bot.on("message", (msg) => {
  if (!botState.isSetting || msg.text === TEXTS.BACK_TO_SETTINGS) return;

  const inputText = msg.text;
  let isValid = false;
  let newValue = "";

  if (botState.isSettingLimit) {
    isValid = limitValidate(inputText);
    if (isValid) {
      newValue = parseInt(inputText);
      botState.settings.limit = newValue;
      botState.isSetting = false;
      botState.isSettingLimit = false;
    } else {
      bot.sendMessage(msg.chat.id, TEXTS.SETTING_LIMIT_INVALID);
    }
  } else if (botState.isSettingLink) {
    isValid = checkExistingChannel(inputText);
    if (isValid) {
      newValue = inputText;
      botState.settings.channel = newValue;
      botState.isSetting = false;
      botState.isSettingLink = false;
    } else {
      bot.sendMessage(msg.chat.id, TEXTS.SETTING_LINK_INVALID);
    }
  } else if (botState.isSettingSpeed) {
    isValid = speedValidate(inputText);
    if (isValid) {
      newValue = parseInt(inputText);
      botState.settings.speed = newValue;
      botState.isSetting = false;
      botState.isSettingSpeed = false;
    } else {
      bot.sendMessage(msg.chat.id, TEXTS.SETTING_SPEED_INVALID);
    }
  }

  if (isValid) {
    bot.sendMessage(msg.chat.id, TEXTS.SETTING_SUCCESS(newValue), {
      reply_markup: {
        keyboard: [
          [TEXTS.SETTING_CURRENT, TEXTS.BACK],
          [TEXTS.SETTING_LINK],
          [TEXTS.SETTING_SPEED],
          [TEXTS.SETTING_LIMIT],
        ],
      },
    });
  } else {
    bot.sendMessage(msg.chat.id, TEXTS.INPUT_AGAIN_OR_BACK, {
      reply_markup: {
        keyboard: [[TEXTS.BACK_TO_SETTINGS]],
      },
    });
  }
});

bot.onText(new RegExp(TEXTS.SHOW_CURRENT_PHONES), (msg) => {
  if (botState.isSetting) return;
  console.log( TEXTS.CURRENT_PHONES())
  bot.sendMessage(msg.chat.id, TEXTS.CURRENT_PHONES(), {
    parse_mode: 'HTML'
  });
})

// DOCUMENT SENDING

bot.onText(new RegExp(TEXTS.LOAD), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.LOAD_FILE, {
    reply_markup: {
      keyboard: [[TEXTS.SHOW_CURRENT_PHONES], [TEXTS.LOAD_FROM_EXCEL, TEXTS.LOAD_FROM_LINK], [TEXTS.BACK]],
    },
  });
});

bot.onText(new RegExp(TEXTS.LOAD_FROM_EXCEL), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.PIN_EXCEL_FILE);
});

bot.onText(new RegExp(TEXTS.LOAD_FROM_LINK), (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.SEND_LINK_ON_FILE);
});

bot.on("document", async (msg) => {
  if (botState.isSetting) return;

  if (msg.document.mime_type !== EXCEL_MIME_TYPE) {
    bot.sendMessage(msg.chat.id, TEXTS.WRONG_FILE);
    return;
  }

  const filePath = await bot.downloadFile(
    msg.document.file_id,
    FILES_SAVE_FOLDER
  );
  bot.sendMessage(msg.chat.id, TEXTS.HADLING);

  const telephones = await parseTelephoneExcel(
    filePath,
    EXCEL_WOOKSHEET_NUMBER,
    EXCEL_COL_NUMBER
  );
  bot.sendMessage(msg.chat.id, TEXTS.GETTED_TELEPHONE_NUMBER);
  bot.sendMessage(msg.chat.id, telephones.join("\n"));

  botState.telephones.push(...telephones);

  fs.unlinkSync(filePath);
});

bot.onText(/^https:\/\/disk\.yandex\.ru/, async (msg) => {
  if (botState.isSetting) return;

  bot.sendMessage(msg.chat.id, TEXTS.LOADING);
  let filePath;
  try {
    filePath = await downloadFileFromYandex(msg.text, FILES_SAVE_FOLDER);
  } catch (e) {
    bot.sendMessage(msg.chat.id, TEXTS.WRONG_LINK);
    return;
  }

  const telephones = await parseTelephoneExcel(
    filePath,
    EXCEL_WOOKSHEET_NUMBER,
    EXCEL_COL_NUMBER
  );
  bot.sendMessage(msg.chat.id, TEXTS.GETTED_TELEPHONE_NUMBER);
  bot.sendMessage(msg.chat.id, telephones.join("\n"));

  botState.telephones.push(...telephones);

  fs.unlinkSync(filePath);
});
