const botState = require("./bot-state");
const { getMinutesWordForm } = require("./utils/words-form");

module.exports = {
  WELCOME: `
Привет! Я бот для добавления пользователей в TG канал
Я могу приглашать пользователей в указанный канал по номерам телефонов. Номера можно загрузить из локального Excel файла или с Яндекс диска. Подробнее см. пункт "Выгрузка телефонных номеров".
  
Перед запуском необходимо установить в настройках канал куда будут приглашаться пользователи.
  `,
  START: "Начать приглашения",
  NO_NUMBER_PHONES: "Телефонов для приглашения не обнаружено",
  LOAD: "Загрузка телефонов 📥",
  LOAD_FROM_EXCEL: "Загрузить данные из Excel",
  LOAD_FROM_LINK: "Загрузить данные по ссылке Яндекс Диска",
  BACK: "Назад ↩",
  INPUT_AGAIN_OR_BACK: "Введите снова или вернитесь назад",
  BACK_TO_SETTINGS: "К настройкам",
  LOAD_FILE:
    `Загрузить файл можно двумя путями: 
1) Отправив публичную ссылку на яндекс диск файлик;
2) Прикрепить файл его к сообщению.`,
  WRONG_FILE: "Это не Excel",
  WRONG_LINK: "Это ссылка не на excel",
  BACK_TEXT: "В главное меню",
  GETTED_TELEPHONE_NUMBER: "Полученные номера телефонов:",
  HADLING: "Анализирую...",
  LOADING: "Подождите...",
  PIN_EXCEL_FILE: "Прикрепите xlsx файл",
  SEND_LINK_ON_FILE: "Отправьте ссылку на файл",
  SHOW_CURRENT_PHONES: "Посмотреть загруженные номера телефонов",
  SETTINGS: "Настройки ⚙",
  SETTING_PROMPT: "Выберите и установите настройки",
  SETTING_CURRENT: "Посмотреть текущие настройки",
  SETTING_SPEED: "Установка скорости приглашения пользователей",
  SETTING_SPEED_DESCRIPTION:
    "Введите временной интервал, через который будут приглашаться пользователи (от 1 до 60)",
  SETTING_SPEED_INVALID: "Некорректные данные. Введите целое число от 1 до 60 включительно",
  SETTING_LINK: "Установка ссылки на канал",
  SETTING_LINK_DESCRIPTION:
    "Напишите id канала в который будут приглашаться пользователи",
  SETTING_LINK_INVALID: "Такого канала не существует",
  SETTING_LIMIT: "Установить суточный лимит приглашений",
  SETTING_LIMIT_DESCRIPTION:
    "Установите суточный лимит пользователей для приглашения в канал (от 1 до 200).",
  SETTING_LIMIT_INVALID: "Некорректные данные. Введите целое число от 1 до 200 включительно",

  SETTING_SUCCESS: (value) => `Значение ${value} успешно установлено`,
  // use function for getting current value every time we use these texts
  SETTINGS_SHOW: () => `Установленые следующие настройки:

Канал куда происходит приглашение: <b>${botState.settings.channel}</b>
Суточный лимит пользователей: <b>${botState.settings.limit}</b>
Пользователи приглашаются раз в <b>${
    botState.settings.speed
  } ${getMinutesWordForm(botState.settings.speed)}.</b>
  `,
  CURRENT_PHONES: () => `Пользователи с этмии номерами ожидают добавления:
${botState.telephones.join("\n")}
  `,
};
