/**
 * Check string on telephone number
 * @param {string} telString String is needed check
 * @returns {boolean} Is giving string telephone number
 */
function checkTelephone(telString) {
  return /^((8|\+?7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(telString);
}

module.exports = {
  checkTelephone,
};
