/** Mime type for xslx files */
const EXCEL_MIME_TYPE =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

module.exports = {
  EXCEL_MIME_TYPE,
};
